<?php

namespace App\Controllers;

use App\Models\CrudModel;

class Crud extends BaseController{

	function index(){

		$crudModel = new CrudModel();

		$data['tbl_product'] = $crudModel->orderBy('code', 'DESC')->paginate(10);

		$data['pagination_link'] = $crudModel->pager;

		return view('crud_view', $data);
	}

	function add(){

		return view('add_data');

	}

	function add_validation()

	{

		helper(['form', 'url']);

		$error = $this->validate([
			'category' => 'required',
			'name' => 'required',
			'price' => 'required',
			'stocks' => 'required',
		]);

		if(!$error)
		{
			echo view('add_data', [
				'error' 	=> $this->validator
			]);

		}
		else
		{

			$crudModel = new CrudModel();

			$crudModel->save([
				'category' => $this->request->getVar('category'),  
				'name' => $this->request->getVar('name'), 
				'price' => $this->request->getVar('price'), 
				'stocks' => $this->request->getVar('stocks') 
			]);

			$session = \Config\Services::session();

			$session->setFlashdata('success','Successfully Added');

			return $this->response->redirect(site_url('/crud'));

		}
	}


	function fetch_single_data($code = null){

		$crudModel = new CrudModel();

		$data['tbl_product'] = $crudModel->where('code', $code)->first();

		return view('edit_data', $data);

	}

	function edit_validation()
	{

		helper (['form', 'url']);

		$error = $this->validate([
			'category' => 'required',
			'name' => 'required',
			'price' => 'required',
			'stocks' => 'required'
		]);

		if(!$error)
		{
			echo view('edit_data'), [
				'error' => $this->validator

			];	

		}
		else 
		{

			$crudModel = new CrudModel();

			$code = $this->request->getVar('code');

			$data = [
				'category' => $this->request->getVar('category'),
				'name' => $this->request->getVar('name'),
				'price' => $this->request->getVar('price'),
				'stocks' => $this->request->getVar('stocks')

			];


			$crudModel->update($code, $data);

			$session = \Config\Services::session();

			$session->setFlashdata('success','Updated Successfully');

			return $this->response->redirect(site_url("/crud"));

		}

	}

	function delete($code)

	{

		$crudModel = new CrudModel();

		$crudModel->where('code', $code)->delete($code);

		$session = \Config\Services::session();

		$session->setFlashdata('success','Successfully Deleted');

		return $this->response->redirect(site_url('/crud'));

	}
	
}

?>