<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
     content="width=device-width, initial-scale=1, user-scalable=yes">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <title>Codeigniter 4 Crud Application</title>
    <!--  -->
</head>
<body>
    <div class="container">
        
        <h2 class="text-center mt-4 mb-4">Codeigniter 4 Crud Application</h2>

        <?php

        $validation = \Config\Services::validation();

        ?>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col">Add Data</div>
                    <div class="col text-right">
                        
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="<?php echo base_url("/crud/add_validation")?>">
                    <div class="form-group">
                        <label>Unit Category</label>
                        <input type="text" name="category" class="form-control" />

                        <?php
                        if($validation->getError('category'))
                        {
                            echo "
                            <div class='alert alert-danger mt-2'>
                            ".$validation->getError('category')."
                            </div>
                            ";
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Unit Name</label>
                        <input type="text" name="name" class="form-control" />
                        <?php
                        if($validation->getError('name'))
                        {
                            echo "
                            <div class='alert alert-danger mt-2'>
                            ".$validation->getError('name')."
                            </div>
                            ";
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Unit Price</label>
                        <input type="text" name="price" class="form-control" />
                        <?php
                        if($validation->getError('price'))
                        {
                            echo "
                            <div class='alert alert-danger mt-2'>
                            ".$validation->getError('price')."
                            </div>
                            ";
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Unit Stocks</label>
                        <input type="text" name="stocks" class="form-control" />
                        <?php
                        if($validation->getError('stocks'))
                        {
                            echo "
                            <div class='alert alert-danger mt-2'>
                            ".$validation->getError('stocks')."
                            </div>
                            ";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
 
</body>
</html>
