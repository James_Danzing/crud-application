<?php

namespace App\Models;

use CodeIgniter\Model;

class CrudModel extends Model{

	protected $table = 'tbl_product';

	protected $primaryKey = 'code';

	protected $allowedFields = ['category','name','price','stocks'];
}

?>